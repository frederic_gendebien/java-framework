/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3;

import org.springframework.http.MediaType;
import org.springframework.util.MimeType;

public final class Headers {

    public static final String CONTENT_TYPE = "content-type";

    public static final String CONTENT_TYPE_JSON = "application/json";

    public static final String CONTENT_TYPE_JSON_API = "application/vnd.api+json";

    public static final String CLIENT_CORRELATION_ID = "x-dim3-client-correlation-id";

    public static final String EVENT_ID = "x-dim3-event-id";

    public static final String EVENT_TYPE = "x-dim3-event-type";

    public static final String SERVER_CORRELATION_ID = "x-dim3-server-correlation-id";

    public static final String SESSION_ID = "x-dim3-session-id";

    public static final String TOKEN = "x-dim3-auth-token";

    public static final String USER = "x-dim3-user";

    public static final MimeType MEDIA_TYPE_JSON_API = MediaType.valueOf(CONTENT_TYPE_JSON_API);

    private Headers() { }
}
