package com.dim3.progress;

import lombok.Data;

@Data
public class ProgressEvent {

    static final String TYPE = "com.dim3.ProgressEvent";

    private final String id;

    private final int progress;

    private final int total;

    public ProgressEvent(String id, int progress, int total) {
        this.id = id;
        this.progress = progress;
        this.total = total;
    }
}
