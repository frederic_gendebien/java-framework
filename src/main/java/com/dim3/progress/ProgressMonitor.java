package com.dim3.progress;

import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.context.ApplicationEventPublisher;

public class ProgressMonitor implements AutoCloseable {

    private static final int DEFAULT_SAMPLING_RATE = 20; // 5%

    private final String id;

    private final int size;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final int publicationThreshold;

    private int current;

    private int progress = 0;

    ProgressMonitor(String id, int size, ApplicationEventPublisher applicationEventPublisher) {
        this.id = id;
        this.size = size;
        this.applicationEventPublisher = applicationEventPublisher;
        this.publicationThreshold = Math.max(1, size / DEFAULT_SAMPLING_RATE);
    }

    @Override
    public void close() {
        applicationEventPublisher.publishEvent(new ProgressEvent(id, size, size));
    }

    public void notifyProgress() {
        notifyProgress(1);
    }

    public void notifyProgress(int increment) {
        current += increment;
        int progress = this.progress;

        if (current >= size) {
            progress = size;
        } else if (current >= this.progress + publicationThreshold){
            progress = (current / DEFAULT_SAMPLING_RATE) * DEFAULT_SAMPLING_RATE;
        }

        if (progress > this.progress) {
            this.progress = progress;
            applicationEventPublisher.publishEvent(new ProgressEvent(id, progress, size));
        }
    }

    public static void withProgressMonitor(ApplicationEventPublisher applicationEventPublisher, String id, int size, Consumer<ProgressMonitor> consumer) {
        try (ProgressMonitor progressMonitor = new ProgressMonitor(id, size, applicationEventPublisher)) {
            consumer.accept(progressMonitor);
        }
    }

    public static <T> T withProgressMonitor(ApplicationEventPublisher applicationEventPublisher, String id, int size, Function<ProgressMonitor, T> function) {
        try (ProgressMonitor progressMonitor = new ProgressMonitor(id, size, applicationEventPublisher)) {
            return function.apply(progressMonitor);
        }
    }
}
