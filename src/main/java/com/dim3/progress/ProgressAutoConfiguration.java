package com.dim3.progress;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@ConditionalOnBean({ ObjectMapper.class, RabbitTemplate.class })
@ConditionalOnProperty(name = "progress.enabled", havingValue = "true", matchIfMissing = true)
@AutoConfigureAfter({ JacksonAutoConfiguration.class, RabbitAutoConfiguration.class })
public class ProgressAutoConfiguration {

    @Bean
    public ProgressEventPublisher progressEventPublisher(RabbitTemplate rabbitTemplate, ObjectMapper objectMapper) {
        return new ProgressEventPublisher(rabbitTemplate, objectMapper);
    }
}
