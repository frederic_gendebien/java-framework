package com.dim3.progress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.event.EventListener;

import com.dim3.Headers;
import com.dim3.context.RequestContext;
import com.dim3.context.RequestContextHolder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

final class ProgressEventPublisher {

    private static final String EXCHANGE = "amq.direct";

    private static final String ROUTING_KEY_FORMAT = ProgressEvent.TYPE + ".%s";

    private final Logger logger = LoggerFactory.getLogger(ProgressEventPublisher.class);

    private final RabbitTemplate rabbitTemplate;

    private final ObjectMapper objectMapper;

    ProgressEventPublisher(RabbitTemplate rabbitTemplate, ObjectMapper objectMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.objectMapper = objectMapper;
    }

    @EventListener
    public void handle(ProgressEvent event) throws JsonProcessingException {
        RequestContext requestContext = RequestContextHolder.getRequestContext();
        String sessionId = (String) requestContext.get(Headers.SESSION_ID);

        if (sessionId == null) {
            logger.warn("No session id found in context");
        } else {
            String routingKey = String.format(ROUTING_KEY_FORMAT, sessionId);
            String convertedMessage = objectMapper.writeValueAsString(event);

            rabbitTemplate.convertAndSend(EXCHANGE, routingKey, convertedMessage, message -> {
                MessageProperties messageProperties = message.getMessageProperties();
                messageProperties.setContentType(MessageProperties.CONTENT_TYPE_JSON);

                return message;
            });
        }
    }
}
