package com.dim3.web.client;

import com.dim3.context.RequestContext;
import com.dim3.context.RequestContextHolder;

final class RequestContextHeaderClientHttpRequestInterceptor extends AbstractHeaderClientHttpRequestInterceptor {

    private final String headerName;

    RequestContextHeaderClientHttpRequestInterceptor(String headerName) {
        super(headerName);

        this.headerName = headerName;
    }

    @Override
    protected String getHeaderValue() {
        RequestContext requestContext = RequestContextHolder.getRequestContext();
        Object headerValue = requestContext.get(headerName);
        String result = null;

        if (headerValue != null) {
            result = String.valueOf(headerValue);
        }

        return result;
    }
}
