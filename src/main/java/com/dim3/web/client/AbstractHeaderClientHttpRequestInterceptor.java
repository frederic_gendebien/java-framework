/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.web.client;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

abstract class AbstractHeaderClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    private final String headerName;

    protected AbstractHeaderClientHttpRequestInterceptor(String headerName) {
        this.headerName = headerName;
    }

    @Override
    public final ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        String headerValue = getHeaderValue();

        if (headerValue != null) {
            HttpHeaders headers = request.getHeaders();
            headers.set(headerName, getHeaderValue());
        }

        return execution.execute(request, body);
    }

    protected abstract String getHeaderValue();
}
