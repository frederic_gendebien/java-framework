package com.dim3.web.client;

import com.dim3.Headers;
import com.dim3.security.User;
import com.dim3.security.UserDetails;
import com.dim3.security.jwt.JsonWebToken;

import java.util.Arrays;
import java.util.UUID;

final class JwtHeaderClientHttpRequestInterceptor extends AbstractHeaderClientHttpRequestInterceptor {

    private final UserDetails userDetails;

    private final String secret;

    private final long validity;

    JwtHeaderClientHttpRequestInterceptor(String applicationName, String secret, long validity) {
        super(Headers.TOKEN);

        userDetails = new User(applicationName, null, UUID.randomUUID().toString(), Arrays.asList("ROLE_SYSTEM"));
        this.secret = secret;
        this.validity = validity;
    }

    @Override
    protected String getHeaderValue() {
        JsonWebToken jsonWebToken = new JsonWebToken(userDetails, secret, validity);

        return jsonWebToken.getPassword();
    }
}
