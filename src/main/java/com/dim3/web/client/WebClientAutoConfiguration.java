/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.web.client;

import com.dim3.Headers;
import com.dim3.security.jwt.JwtProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Configuration
@ConditionalOnBean(RestTemplate.class)
public class WebClientAutoConfiguration {

    @Bean
    public RestTemplateCustomizer interceptorCustomizer(JwtProperties jwtProperties, @Value("${spring.application.name}") String applicationName) {
        return restTemplate -> restTemplate.setInterceptors(Arrays.asList(
                new JwtHeaderClientHttpRequestInterceptor(applicationName, jwtProperties.getSecret(), jwtProperties.getValidity()),
                new RequestContextHeaderClientHttpRequestInterceptor(Headers.CLIENT_CORRELATION_ID),
                new RequestContextHeaderClientHttpRequestInterceptor(Headers.SERVER_CORRELATION_ID),
                new RequestContextHeaderClientHttpRequestInterceptor(Headers.SESSION_ID),
                new UserHeaderClientHttpRequestInterceptor(jwtProperties.getSecret(), jwtProperties.getValidity())));
    }
}
