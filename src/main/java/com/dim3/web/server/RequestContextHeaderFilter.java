package com.dim3.web.server;

import com.dim3.context.RequestContext;
import com.dim3.context.RequestContextHolder;

final class RequestContextHeaderFilter extends AbstractHeaderFilter {

    private final String headerName;

    RequestContextHeaderFilter(String headerName) {
        super(headerName);

        this.headerName = headerName;
    }

    @Override
    protected void handleHeader(String headerValue) {
        RequestContext requestContext = RequestContextHolder.getRequestContext();
        requestContext.set(headerName, headerValue);
    }

    @Override
    protected void cleanUp() {
        RequestContext requestContext = RequestContextHolder.getRequestContext();
        requestContext.remove(headerName);
    }
}
