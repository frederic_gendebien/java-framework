/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.web.server;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.dim3.Headers;
import com.dim3.jsonapi.JsonApiAutoConfiguration;
import com.dim3.jsonapi.JsonApiHttpMessageConverter;
import com.github.jasminb.jsonapi.ResourceConverter;

@Configuration
@ConditionalOnBean(annotation = { Controller.class, RestController.class })
@AutoConfigureAfter(JsonApiAutoConfiguration.class)
public class WebServerAutoConfiguration {

    @Bean
    @Order(Ordered.LOWEST_PRECEDENCE - 1)
    public RequestContextFilter requestContextFilter() {
        return new RequestContextFilter();
    }

    @Bean
    public RequestContextHeaderFilter clientCorrelationIdFilter() {
        return new RequestContextHeaderFilter(Headers.CLIENT_CORRELATION_ID);
    }

    @Bean
    public RequestContextHeaderFilter serverCorrelationIdFilter() {
        return new RequestContextHeaderFilter(Headers.SERVER_CORRELATION_ID);
    }

    @Bean
    public RequestContextHeaderFilter sessionIdFilter() {
        return new RequestContextHeaderFilter(Headers.SESSION_ID);
    }

    @Bean
    public RequestContextHeaderFilter userFilter() {
        return new RequestContextHeaderFilter(Headers.USER);
    }

    @Configuration
    @ConditionalOnClass(ResourceConverter.class)
    protected static class JsonApiConverterConfiguration {

        @Bean
        public JsonApiHttpMessageConverter jsonApiObjectConverter(ResourceConverter resourceConverter) {
            return new JsonApiHttpMessageConverter(resourceConverter);
        }
    }

    @Bean
    @ConditionalOnProperty(name = "com.dim3.security.cors-enabled", havingValue = "true", matchIfMissing = true)
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods(HttpMethod.GET.name(),
                                HttpMethod.DELETE.name(),
                                HttpMethod.HEAD.name(),
                                HttpMethod.PATCH.name(),
                                HttpMethod.POST.name(),
                                HttpMethod.PUT.name());
            }
        };
    }
}
