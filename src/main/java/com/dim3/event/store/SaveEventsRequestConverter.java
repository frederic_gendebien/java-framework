/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.store;

import java.io.IOException;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import com.dim3.event.registry.EventRegistry;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

final class SaveEventsRequestConverter extends AbstractHttpMessageConverter<SaveEventsRequest> {

    private final Logger logger = LoggerFactory.getLogger(SaveEventsRequestConverter.class);

    private final EventRegistry eventRegistry;

    private final ObjectMapper objectMapper;

    SaveEventsRequestConverter(EventRegistry eventRegistry, ObjectMapper objectMapper) {
        super(MediaType.APPLICATION_JSON);

        this.eventRegistry = eventRegistry;
        this.objectMapper = objectMapper;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return SaveEventsRequest.class.isAssignableFrom(clazz);
    }

    @Override
    protected boolean canRead(MediaType mediaType) {
        return false;
    }

    @Override
    protected SaveEventsRequest readInternal(Class<? extends SaveEventsRequest> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void writeInternal(SaveEventsRequest saveEventsRequest, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        OutputStream outputStream = outputMessage.getBody();
        ObjectNode rootNode = objectMapper.createObjectNode();
        rootNode.put("version", saveEventsRequest.getVersion());
        ArrayNode eventsNode = objectMapper.createArrayNode();

        for (Object event : saveEventsRequest.getEvents()) {
            ObjectNode eventNode = objectMapper.valueToTree(event);
            Class<?> eventClass = event.getClass();
            String eventType = eventRegistry.getType(eventClass);
            eventNode.put("@type", eventType);
            eventsNode.add(eventNode);
        }

        rootNode.set("events", eventsNode);
        objectMapper.writeValue(outputStream, rootNode);
        logMessage(eventsNode);
    }

    private void logMessage(ArrayNode eventsNode) {
        if (logger.isInfoEnabled()) {
            for (int i = 0; i < eventsNode.size(); i++) {
                JsonNode eventNode = eventsNode.get(i);
                String type = eventNode.get("@type").asText();
                String id = "n/a";
                JsonNode idNode = eventNode.get("id");

                if (idNode != null) {
                    id = idNode.asText();
                }

                logger.info(">>> {} - {} - HTTP", type, id);
            }
        }
    }
}
