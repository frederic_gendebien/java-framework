/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.store;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;

import org.springframework.boot.autoconfigure.web.client.RestTemplateAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.dim3.event.registry.EnableEventRegistry;
import com.dim3.event.registry.EventRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@ConditionalOnBean(annotation = EnableEventStore.class)
@AutoConfigureAfter(RestTemplateAutoConfiguration.class)
@EnableConfigurationProperties(EventStoreProperties.class)
public class EventStoreAutoConfiguration {

    @Bean
    public GetEventsResponseConverter getEventsResponseConverter(EventRegistry eventRegistry, ObjectMapper objectMapper) {
        return new GetEventsResponseConverter(eventRegistry, objectMapper);
    }

    @Bean
    public SaveEventsRequestConverter saveEventsRequestConverter(EventRegistry eventRegistry, ObjectMapper objectMapper) {
        return new SaveEventsRequestConverter(eventRegistry, objectMapper);
    }

    @Bean
    public EventStore eventStore(EventStoreProperties properties, RestTemplate restTemplate) {
        return new EventStoreImpl(properties.getHost(), properties.getPort(), restTemplate);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }

    @Configuration
    @EnableEventRegistry("com.dim3")
    @ConditionalOnMissingBean(EventRegistry.class)
    protected static class EventRegistryConfiguration {

    }
}
