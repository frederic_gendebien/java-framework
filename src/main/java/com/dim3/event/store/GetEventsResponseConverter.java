/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.store;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;

import com.dim3.event.registry.EventRegistry;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

final class GetEventsResponseConverter extends AbstractHttpMessageConverter<GetEventsResponse> {

    private final EventRegistry eventRegistry;

    private final ObjectMapper objectMapper;

    GetEventsResponseConverter(EventRegistry eventRegistry, ObjectMapper objectMapper) {
        super(MediaType.APPLICATION_JSON);

        this.eventRegistry = eventRegistry;
        this.objectMapper = objectMapper;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return GetEventsResponse.class.isAssignableFrom(clazz);
    }

    @Override
    protected boolean canWrite(MediaType mediaType) {
        return false;
    }

    @Override
    protected GetEventsResponse readInternal(Class<? extends GetEventsResponse> clazz, HttpInputMessage inputMessage) throws IOException {
        InputStream inputStream = inputMessage.getBody();
        JsonNode rootNode = objectMapper.readTree(inputStream);
        JsonNode eventNodes = rootNode.get("events");
        List<Object> events = new ArrayList<>();

        for (JsonNode eventNode : eventNodes) {
            if (eventNode instanceof ObjectNode) {
                ObjectNode objectNode = (ObjectNode) eventNode;
                String type = eventNode.get("@type").asText();
                objectNode.remove("@type");
                Class<?> eventClass = eventRegistry.getClass(type);
                Object event = objectMapper.treeToValue(objectNode, eventClass);
                events.add(event);
            }
        }

        return new GetEventsResponse(events);
    }

    @Override
    protected void writeInternal(GetEventsResponse getEventsResponse, HttpOutputMessage outputMessage) {
        throw new UnsupportedOperationException();
    }
}
