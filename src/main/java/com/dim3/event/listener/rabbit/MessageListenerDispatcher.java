/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.rabbit;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

import com.dim3.context.RequestContextHolder;
import com.dim3.event.listener.EventListener;
import com.dim3.event.listener.idempotency.IdempotencyTemplate;
import com.dim3.event.registry.EventRegistry;
import com.dim3.exception.Bug;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

final class MessageListenerDispatcher implements MessageListener {

    private final Logger logger = LoggerFactory.getLogger(MessageListenerDispatcher.class);

    private final Map<Object, EventListeners> eventListeners = new HashMap<>();

    private final EventRegistry eventRegistry;

    private final ObjectMapper objectMapper;

    private final IdempotencyTemplate idempotencyTemplate;

    MessageListenerDispatcher(EventRegistry eventRegistry, ObjectMapper objectMapper, IdempotencyTemplate idempotencyTemplate) {
        this.eventRegistry = eventRegistry;
        this.objectMapper = objectMapper;
        this.idempotencyTemplate = idempotencyTemplate;
    }

    void add(Object bean, Method method, String eventType, EventListener eventListenerAnnotation) {
        EventListeners eventListeners = this.eventListeners.get(eventType);

        if (eventListeners == null) {
            eventListeners = new EventListeners(idempotencyTemplate);
            this.eventListeners.put(eventType, eventListeners);
        }

        eventListeners.add(bean, method, eventListenerAnnotation);
    }

    @Override
    public void onMessage(Message message) {
        byte[] body = message.getBody();

        try {
            JsonNode node = objectMapper.readTree(body);
            ObjectNode eventNode = objectMapper.convertValue(node, ObjectNode.class);
            String eventType = eventNode.get("@type").asText();

            if (eventType == null) {
                throw new Bug("Receveid a message with no type: " + message.toString());
            }

            EventListeners eventListeners = this.eventListeners.get(eventType);

            if (eventListeners != null) {
                Class<?> eventClass = eventRegistry.getClass(eventType);
                eventNode.remove("@type");
                Object event = objectMapper.treeToValue(eventNode, eventClass);
                logMessage(eventNode, eventType);
                eventListeners.notify(event);
            }
        } catch (IOException | InvocationTargetException | IllegalAccessException e ) {
            throw new Bug("Unable to notify listener", e);
        } finally {
            RequestContextHolder.clearRequestContext();
        }
    }

    private void logMessage(ObjectNode eventNode, String eventType) {
        if (logger.isInfoEnabled()) {
            String id = "n/a";
            JsonNode idNode = eventNode.get("id");

            if (idNode != null) {
                id = idNode.asText();
            }

            logger.info("<<< {} - {} - RabbitMQ", eventType, id);
        }
    }
}
