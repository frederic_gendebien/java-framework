/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.rabbit;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.dim3.Headers;
import com.dim3.security.jwt.JwtAuthenticationToken;

final class JwtInboundMessagePostProcessor extends AbstractInboundHeaderMessagePostProcessor {

    private final AuthenticationManager authenticationManager;

    JwtInboundMessagePostProcessor(AuthenticationManager authenticationManager) {
        super(Headers.TOKEN);

        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void handleHeader(Object headerValue) {
        String header = String.valueOf(headerValue);
        JwtAuthenticationToken token = new JwtAuthenticationToken(header);
        Authentication authentication = authenticationManager.authenticate(token);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);
    }
}
