/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.rabbit;

import com.dim3.context.RequestContext;
import com.dim3.context.RequestContextHolder;

final class RequestContextInboundHeaderMessagePostProcessor extends AbstractInboundHeaderMessagePostProcessor {

    private final String headerName;

    RequestContextInboundHeaderMessagePostProcessor(String headerName) {
        super(headerName);

        this.headerName = headerName;
    }

    @Override
    protected void handleHeader(Object headerValue) {
        RequestContext requestContext = RequestContextHolder.getRequestContext();
        requestContext.set(headerName, headerValue);
    }
}
