/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.rabbit;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import com.dim3.event.Event;
import com.dim3.event.listener.EventListener;
import com.dim3.exception.Bug;

final class EventListenerAnnotationBeanPostProcessor implements BeanPostProcessor {

    private final MessageListenerDispatcher listener;

    EventListenerAnnotationBeanPostProcessor(MessageListenerDispatcher listener) {
        this.listener = listener;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> clazz = AopUtils.getTargetClass(bean);
        ReflectionUtils.doWithMethods(clazz, method -> {
            EventListener eventListener = AnnotationUtils.findAnnotation(method, EventListener.class);

            if (eventListener != null) {
                int parameterCount = method.getParameterCount();

                if (parameterCount != 1) {
                    throw new Bug("@EventListener methods must have a single parameter: " + method);
                }

                Class<?> returnType = method.getReturnType();

                if (Void.TYPE != returnType && !Runnable.class.isAssignableFrom(returnType)) {
                    throw new Bug("@EventListener methods must either return a Runnable or no value at all: " + method);
                }

                Class<?> parameterType = method.getParameterTypes()[0];
                Event event = AnnotationUtils.findAnnotation(parameterType, Event.class);

                if (event == null) {
                    throw new Bug("@EventListener method parameter type must be annotated with @Event: " + method);
                }

                String type = event.value();
                listener.add(bean, method, type, eventListener);
            }
        });

        return bean;
    }
}
