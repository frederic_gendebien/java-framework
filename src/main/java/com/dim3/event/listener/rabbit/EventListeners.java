/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.rabbit;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;

import com.dim3.event.listener.EventListener;
import com.dim3.event.listener.idempotency.IdempotencyTemplate;

final class EventListeners {

    private final Collection<EventListenerMethod> eventListenerMethods = new HashSet<>();

    private final IdempotencyTemplate idempotencyTemplate;

    EventListeners(IdempotencyTemplate idempotencyTemplate) {
        this.idempotencyTemplate = idempotencyTemplate;
    }

    void add(Object bean, Method method, EventListener eventListenerAnnotation) {
        EventListenerMethod eventListenerMethod = new EventListenerMethod(bean, method, idempotencyTemplate, eventListenerAnnotation);
        eventListenerMethods.add(eventListenerMethod);
    }

    void notify(Object event) throws InvocationTargetException, IllegalAccessException {
        for (EventListenerMethod eventListenerMethod : eventListenerMethods) {
            eventListenerMethod.notify(event);
        }
    }
}
