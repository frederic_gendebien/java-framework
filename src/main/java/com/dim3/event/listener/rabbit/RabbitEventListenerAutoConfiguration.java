/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;

import com.dim3.Headers;
import com.dim3.event.listener.idempotency.IdempotencyTemplate;
import com.dim3.event.rabbit.RabbitMQ;
import com.dim3.event.registry.EnableEventRegistry;
import com.dim3.event.registry.EventRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;

@Configuration
@ConditionalOnClass({ RabbitTemplate.class, Channel.class })
@AutoConfigureAfter(TransactionAutoConfiguration.class)
public class RabbitEventListenerAutoConfiguration {

    private static final String DEAD_LETTER_ARGUMENT_NAME = "x-dead-letter-exchange";

    private static final String DEAD_LETTER_EXCHANGE_NAME = "dead-letter";

    private final IdempotencyTemplate idempotencyTemplate;

    public RabbitEventListenerAutoConfiguration(@Autowired(required = false) IdempotencyTemplate idempotencyTemplate) {
        this.idempotencyTemplate = idempotencyTemplate;
    }

    @Value("${spring.application.name}")
    private String applicationName;

    @Bean
    public EventListenerAnnotationBeanPostProcessor eventListenerAnnotationBeanPostProcessor(EventRegistry eventRegistry, ObjectMapper objectMapper) {
        return new EventListenerAnnotationBeanPostProcessor(rabbitMessageListenerDelegate(eventRegistry, objectMapper));
    }

    @Bean
    public Exchange deadLetterExchange() {
        return ExchangeBuilder.fanoutExchange(DEAD_LETTER_EXCHANGE_NAME)
                .durable(true)
                .build();
    }

    @Bean
    public Queue queue() {
        return QueueBuilder.durable(applicationName)
                .withArgument(DEAD_LETTER_ARGUMENT_NAME, DEAD_LETTER_EXCHANGE_NAME)
                .build();
    }

    @Bean
    public FanoutExchange defaultFanoutExchange() {
        return new FanoutExchange(RabbitMQ.FANOUT_EXCHANGE, true, false);
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue()).to(defaultFanoutExchange());
    }

    @Bean
    public MessageListenerDispatcher rabbitMessageListenerDelegate(EventRegistry eventRegistry, ObjectMapper objectMapper) {
        return new MessageListenerDispatcher(eventRegistry, objectMapper, idempotencyTemplate);
    }

    @Bean
    public MessageListenerAdapter messageListenerAdapter(EventRegistry eventRegistry, ObjectMapper objectMapper) {
        return new MessageListenerAdapter(rabbitMessageListenerDelegate(eventRegistry, objectMapper));
    }

    @Bean
    public JwtInboundMessagePostProcessor jwtInboundMessagePostProcessor(AuthenticationManager authenticationManager) {
        return new JwtInboundMessagePostProcessor(authenticationManager);
    }

    @Bean
    public RequestContextInboundHeaderMessagePostProcessor clientCorrelationIdInboundMessagePostProcessor() {
        return new RequestContextInboundHeaderMessagePostProcessor(Headers.CLIENT_CORRELATION_ID);
    }

    @Bean
    public RequestContextInboundHeaderMessagePostProcessor serverCorrelationIdInboundMessagePostProcessor() {
        return new RequestContextInboundHeaderMessagePostProcessor(Headers.SERVER_CORRELATION_ID);
    }

    @Bean
    public RequestContextInboundHeaderMessagePostProcessor eventIdInboundMessagePostProcessor() {
        return new RequestContextInboundHeaderMessagePostProcessor(Headers.EVENT_ID);
    }

    @Bean
    public RequestContextInboundHeaderMessagePostProcessor userInboundMessagePostProcessor() {
        return new RequestContextInboundHeaderMessagePostProcessor(Headers.USER);
    }

    @Bean
    public RequestContextInboundHeaderMessagePostProcessor sessionIdInboundMessagePostProcessor() {
        return new RequestContextInboundHeaderMessagePostProcessor(Headers.SESSION_ID);
    }

    @Bean
    public SimpleMessageListenerContainer messageListenerContainer(ConnectionFactory connectionFactory, EventRegistry eventRegistry, ObjectMapper objectMapper, AbstractInboundHeaderMessagePostProcessor... afterReceiveMessagePostProcessors) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setQueues(queue());
        container.setMessageListener(messageListenerAdapter(eventRegistry, objectMapper));
        container.setAfterReceivePostProcessors(afterReceiveMessagePostProcessors);
        container.setDefaultRequeueRejected(false);

        return container;
    }

    @Configuration
    @EnableEventRegistry
    @ConditionalOnMissingBean(EventRegistry.class)
    protected static class EnableEventRegistryConfiguration {

    }
}
