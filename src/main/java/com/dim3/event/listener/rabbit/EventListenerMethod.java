/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.rabbit;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.dim3.Headers;
import com.dim3.context.RequestContext;
import com.dim3.context.RequestContextHolder;
import com.dim3.event.listener.EventListener;
import com.dim3.event.listener.idempotency.IdempotencyTemplate;
import com.dim3.exception.Bug;

final class EventListenerMethod {

    private final Object bean;

    private final Method method;

    private final boolean idempotent;

    private final IdempotencyTemplate idempotencyTemplate;

    private final String idempotencyDomain;

    EventListenerMethod(Object bean, Method method, IdempotencyTemplate idempotencyTemplate, EventListener eventListenerAnnotation) {
        this.bean = bean;
        this.method = method;
        idempotent = eventListenerAnnotation.idempotent();

        if (idempotent && idempotencyTemplate == null) {
            throw new Bug("Event listener method is idempotent and no IdempotencyTemplate is present: " + bean.getClass() + "." + method.getName());
        }

        this.idempotencyTemplate = idempotencyTemplate;
        this.idempotencyDomain = eventListenerAnnotation.idempotencyDomain();
    }

    void notify(Object event) throws InvocationTargetException, IllegalAccessException {
        Object result;

        if (idempotent) {
            RequestContext requestContext = RequestContextHolder.getRequestContext();
            long eventId = (long) requestContext.get(Headers.EVENT_ID);
            result = idempotencyTemplate.doWithIdempotency(idempotencyDomain, eventId, () -> {
                try {
                    return method.invoke(bean, event);
                } catch (IllegalAccessException e) {
                    throw new Bug("EventListener method is inaccessible", e);
                } catch (InvocationTargetException e) {
                    Throwable target = e.getTargetException();

                    if (target instanceof Error) {
                        throw (Error) target;
                    } else if (target instanceof RuntimeException) {
                        throw (RuntimeException) target;
                    } else {
                        throw new Bug("Unexpected exception thrown", target);
                    }
                }
            });
        } else {
            result = method.invoke(bean, event);
        }

        if (result instanceof Runnable) {
            Runnable callback = (Runnable) result;
            callback.run();
        }
    }
}
