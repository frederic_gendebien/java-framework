/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.idempotency.jdbc;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.transaction.support.TransactionTemplate;

import com.dim3.event.listener.idempotency.IdempotencyTemplate;

@Configuration
@ConditionalOnClass({ TransactionTemplate.class, JdbcOperations.class })
@ConditionalOnBean({ TransactionTemplate.class, JdbcOperations.class })
@AutoConfigureAfter({ TransactionAutoConfiguration.class, JdbcTemplateAutoConfiguration.class })
@EnableConfigurationProperties(JdbcIdempotencyTemplateProperties.class)
public class JdbcIdempotencyTemplateAutoConfiguration {

    @Bean
    public IdempotencyTemplate idempotencyTemplate(TransactionTemplate transactionTemplate, JdbcOperations jdbcTemplate, JdbcIdempotencyTemplateProperties properties) {
        return new JdbcIdempotencyTemplate(transactionTemplate, jdbcTemplate, properties);
    }
}
