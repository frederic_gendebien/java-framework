/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.listener.idempotency.jdbc;

import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.transaction.support.TransactionTemplate;

import com.dim3.event.listener.idempotency.IdempotencyTemplate;

final class JdbcIdempotencyTemplate implements IdempotencyTemplate {

    private static final String MARK_EVENT_AS_HANDLED_QUERY_PATTERN = "INSERT INTO %s(domain, event) VALUES(?, ?)";

    private final Logger logger = LoggerFactory.getLogger(JdbcIdempotencyTemplate.class);

    private final TransactionTemplate transactionTemplate;

    private final JdbcOperations jdbcTemplate;

    private final String query;

    public JdbcIdempotencyTemplate(TransactionTemplate transactionTemplate, JdbcOperations jdbcTemplate, JdbcIdempotencyTemplateProperties properties) {
        this.transactionTemplate = transactionTemplate;
        this.jdbcTemplate = jdbcTemplate;
        query = String.format(MARK_EVENT_AS_HANDLED_QUERY_PATTERN, properties.getTable());
    }

    @Override
    public <T> T doWithIdempotency(String idempotencyDomain, long eventId, Supplier<T> supplier) {
        return transactionTemplate.execute(status -> {
            T result = null;
            boolean duplicate = false;

            try {
                jdbcTemplate.update(query, idempotencyDomain, eventId);
            } catch (DuplicateKeyException e) {
                duplicate = true;
                logger.debug("Duplicate message {} received for domain {}", eventId, idempotencyDomain);
            }

            if (!duplicate) {
                result = supplier.get();
            }

            return result;
        });
    }
}
