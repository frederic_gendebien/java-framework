/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.registry;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.StringUtils;

import com.dim3.event.Event;
import com.dim3.exception.Bug;

public class EventRegistryRegistrar implements ImportBeanDefinitionRegistrar, EnvironmentAware, ResourceLoaderAware, BeanFactoryAware {

    private final Logger logger = LoggerFactory.getLogger(EventRegistryRegistrar.class);

    private BeanFactory beanFactory;

    private Environment environment;

    private ResourceLoader resourceLoader;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    private void handleBeanDefinition(BeanDefinition beanDefinition, Map<String, Class<?>> eventClasses) {
        if (beanDefinition instanceof ScannedGenericBeanDefinition) {
            ScannedGenericBeanDefinition scannedGenericBeanDefinition = (ScannedGenericBeanDefinition) beanDefinition;
            AnnotationMetadata annotationMetadata = scannedGenericBeanDefinition.getMetadata();
            Map<String, Object> annotationAttributeMap = annotationMetadata.getAnnotationAttributes(Event.class.getName());
            AnnotationAttributes annotationAttributes = AnnotationAttributes.fromMap(annotationAttributeMap);
            String eventType = annotationAttributes.getString("value");
            String eventClassName = scannedGenericBeanDefinition.getBeanClassName();
            Class<?> eventClass = null;

            try {
                eventClass = Class.forName(eventClassName);
            } catch (ClassNotFoundException e) {
                throw new Bug("Event class not found", e);
            }

            eventClasses.put(eventType, eventClass);
        }
    }

    private Set<BeanDefinition> findEvents(AnnotationMetadata importingClassMetadata) {
        Set<BeanDefinition> beanDefinitions = new HashSet<>();
        Map<String, Object> annotationAttributeMap = importingClassMetadata.getAnnotationAttributes(EnableEventRegistry.class.getName());
        AnnotationAttributes annotationAttributes = AnnotationAttributes.fromMap(annotationAttributeMap);
        Set<String> basePackages = new HashSet<>();
        String explicitBasePackage = annotationAttributes.getString("value");

        if (StringUtils.isEmpty(explicitBasePackage)) {
            if (AutoConfigurationPackages.has(beanFactory)) {
                basePackages.addAll(AutoConfigurationPackages.get(beanFactory));
            } else {
                logger.warn("No basePackage specified for @EnableEventRegistry and @EnableAutoConfiguration not detected - no event registry will be created");
            }
        } else {
            basePackages.add(explicitBasePackage);
        }

        if (!basePackages.isEmpty()) {
            ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false, environment);
            scanner.setResourceLoader(resourceLoader);
            scanner.addIncludeFilter(new AnnotationTypeFilter(Event.class));

            for (String basePackage : basePackages) {
                beanDefinitions.addAll(scanner.findCandidateComponents(basePackage));
            }
        }

        return beanDefinitions;
    }

    private static void createEventRegistryBeanDefinition(BeanDefinitionRegistry registry, Map<String, Class<?>> eventClasses) {
        ConstructorArgumentValues constructorArgumentValues = new ConstructorArgumentValues();
        constructorArgumentValues.addGenericArgumentValue(eventClasses, Map.class.getName());
        RootBeanDefinition beanDefinition = new RootBeanDefinition(EventRegistry.class, constructorArgumentValues, null);
        registry.registerBeanDefinition("eventRegistry", beanDefinition);
    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        Map<String, Class<?>> eventClasses = new HashMap<>();
        Set<BeanDefinition> beanDefinitions = findEvents(importingClassMetadata);

        for (BeanDefinition beanDefinition : beanDefinitions) {
            handleBeanDefinition(beanDefinition, eventClasses);
        }

        if (!eventClasses.isEmpty()) {
            createEventRegistryBeanDefinition(registry, eventClasses);
        }
    }
}
