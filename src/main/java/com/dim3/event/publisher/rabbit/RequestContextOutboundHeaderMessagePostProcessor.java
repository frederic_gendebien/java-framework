package com.dim3.event.publisher.rabbit;

import com.dim3.context.RequestContext;
import com.dim3.context.RequestContextHolder;

final class RequestContextOutboundHeaderMessagePostProcessor extends AbstractOutboundHeaderMessagePostProcessor {

    private final String headerName;

    RequestContextOutboundHeaderMessagePostProcessor(String headerName) {
        super(headerName);

        this.headerName = headerName;
    }

    @Override
    protected Object getHeaderValue() {
        RequestContext requestContext = RequestContextHolder.getRequestContext();

        return requestContext.get(headerName);
    }
}
