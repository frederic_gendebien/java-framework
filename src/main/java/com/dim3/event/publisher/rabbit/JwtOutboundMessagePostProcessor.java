/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.publisher.rabbit;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.dim3.Headers;
import com.dim3.security.UserDetails;
import com.dim3.security.jwt.JsonWebToken;
import com.dim3.security.jwt.JwtAuthenticationToken;

final class JwtOutboundMessagePostProcessor extends AbstractOutboundHeaderMessagePostProcessor {

    private final String secret;

    private final long validity;

    JwtOutboundMessagePostProcessor(String secret, long validity) {
        super(Headers.TOKEN);

        this.secret = secret;
        this.validity = validity;
    }

    @Override
    protected Object getHeaderValue() {
        Object result = null;
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            if (authentication instanceof JwtAuthenticationToken) {
                JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
                result = jwtAuthenticationToken.getCredentials();
            } else {
                Object principal = authentication.getPrincipal();

                if (principal instanceof UserDetails) {
                    UserDetails userDetails = (UserDetails) principal;
                    JsonWebToken jsonWebToken = new JsonWebToken(userDetails, secret, validity);
                    result = jsonWebToken.getPassword();
                }
            }
        }

        return result;
    }
}
