/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.publisher.rabbit;

import com.dim3.Headers;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dim3.event.registry.EnableEventRegistry;
import com.dim3.event.registry.EventRegistry;
import com.dim3.security.jwt.JwtAutoConfiguration;
import com.dim3.security.jwt.JwtProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@ConditionalOnBean(annotation = EnableRabbitEventPublisher.class)
@AutoConfigureAfter(JwtAutoConfiguration.class)
public class RabbitEventPublisherAutoConfiguration {

    @Autowired
    private JwtProperties jwtProperties;

    @Bean
    public ContentTypeMessagePostProcessor contentTypeMessagePostProcessor() {
        return new ContentTypeMessagePostProcessor();
    }

    @Bean
    public RequestContextOutboundHeaderMessagePostProcessor clientCorrelationIdOutboundMessagePostProcessor() {
        return new RequestContextOutboundHeaderMessagePostProcessor(Headers.CLIENT_CORRELATION_ID);
    }

    @Bean
    public RequestContextOutboundHeaderMessagePostProcessor serverCorrelationOutboundMessagePostProcessor() {
        return new RequestContextOutboundHeaderMessagePostProcessor(Headers.SERVER_CORRELATION_ID);
    }

    @Bean
    public RequestContextOutboundHeaderMessagePostProcessor userOutboundMessagePostProcessor() {
        return new RequestContextOutboundHeaderMessagePostProcessor(Headers.USER);
    }

    @Autowired
    public void customizeRabbitTemplate(RabbitTemplate rabbitTemplate, AbstractOutboundHeaderMessagePostProcessor... beforePublishPostProcessors) {
        rabbitTemplate.setBeforePublishPostProcessors(beforePublishPostProcessors);
    }

    @Bean
    public RabbitEventPublisher eventPublisher(EventRegistry eventRegistry, ObjectMapper objectMapper, RabbitTemplate rabbitTemplate) {
        return new RabbitEventPublisher(eventRegistry, objectMapper, rabbitTemplate);
    }

    @Bean
    public JwtOutboundMessagePostProcessor jwtOutboundMessagePostProcessor() {
        return new JwtOutboundMessagePostProcessor(jwtProperties.getSecret(), jwtProperties.getValidity());
    }

    @Configuration
    @EnableEventRegistry
    @ConditionalOnMissingBean(EventRegistry.class)
    protected static class EnableEventRegistryConfiguration {

    }
}
