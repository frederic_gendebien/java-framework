/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.event.publisher.rabbit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import com.dim3.event.publisher.EventPublisher;
import com.dim3.event.registry.EventRegistry;
import com.dim3.exception.Bug;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

final class RabbitEventPublisher implements EventPublisher {

    private final Logger logger = LoggerFactory.getLogger(RabbitEventPublisher.class);

    private final EventRegistry eventRegistry;

    private final ObjectMapper objectMapper;

    private final RabbitTemplate rabbitTemplate;

    RabbitEventPublisher(EventRegistry eventRegistry, ObjectMapper objectMapper, RabbitTemplate rabbitTemplate) {
        this.eventRegistry = eventRegistry;
        this.objectMapper = objectMapper;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void publish(Object event) {
        Class<?> eventClass = event.getClass();
        String eventType = eventRegistry.getType(eventClass);

        if (eventType == null) {
            throw new Bug("Unable to publish unsupported event: " + event);
        }

        ObjectNode node = objectMapper.valueToTree(event);
        node.put("@type", eventType);
        byte[] body;

        try {
            body = objectMapper.writeValueAsBytes(node);
        } catch (JsonProcessingException e) {
            throw new Bug("Unable to serialize event: " + event, e);
        }

        rabbitTemplate.convertAndSend("amq.fanout", null, body);

        if (logger.isInfoEnabled()) {
            String id = "n/a";
            JsonNode idNode = node.get("id");

            if (idNode != null) {
                id = idNode.asText();
            }

            logger.info(">>> {} - {} - RabbitMQ", eventType, id);
        }
    }
}
