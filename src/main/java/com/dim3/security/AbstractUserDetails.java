/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.security;

import java.util.Collection;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public abstract class AbstractUserDetails implements UserDetails {

    @Override
    public final Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<? extends GrantedAuthority> result = getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role))
                .collect(Collectors.toSet());

        return result;
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder()
                .append(getUsername())
                .toHashCode();
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof UserDetails)) {
            return false;
        }

        UserDetails u = (UserDetails) obj;

        return new EqualsBuilder()
                .append(getUsername(), u.getUsername())
                .isEquals();
    }
}
