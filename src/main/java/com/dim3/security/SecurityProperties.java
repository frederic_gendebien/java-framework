package com.dim3.security;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties("com.dim3.security")
@Data
public class SecurityProperties {

    private boolean corsEnabled = true;
}
