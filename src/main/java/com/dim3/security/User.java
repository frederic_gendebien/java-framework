/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public final class User extends AbstractUserDetails {

    private final String username;

    private final String password;

    private final String language = "en";

    private final String userId;

    private final Collection<String> roles = new ArrayList<>();

    public User(String username, String password) {
        this(username, password, null, null);
    }

    public User(String username, String password, String userId, Collection<String> roles) {
        this.username = username;
        this.password = password;
        this.userId = userId;

        if (roles != null) {
            this.roles.addAll(roles);
        }
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public Collection<String> getRoles() {
        return Collections.unmodifiableCollection(roles);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
