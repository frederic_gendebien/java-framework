/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.security.jwt;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dim3.Headers;
import com.dim3.exception.Bug;
import com.dim3.security.UserDetails;

@RequestMapping(value = "/api")
public class JwtAuthenticationController {

    private static final String IDENTIFICATION_FIELD = "username";

    private static final String PASSWORD_FIELD = "password";

    private final AuthenticationManager authenticationManager;

    private final JwtProperties jwtProperties;

    public JwtAuthenticationController(AuthenticationManager authenticationManager, JwtProperties jwtProperties) {
        this.authenticationManager = authenticationManager;
        this.jwtProperties = jwtProperties;
    }

    private ResponseEntity createResponse(UserDetails userDetails) {
        String secret = jwtProperties.getSecret();
        long validity = jwtProperties.getValidity();
        JsonWebToken token = new JsonWebToken(userDetails, secret, validity);
        String header = token.getPassword();
        Map<String, String> body = new HashMap<>();
        body.put(Headers.TOKEN, header);

        return ResponseEntity.ok()
                .body(body);
    }

    private ResponseEntity createResponse(Authentication authentication) {
        ResponseEntity response;
        Authentication newAuthentication = authenticationManager.authenticate(authentication);

        if (newAuthentication.isAuthenticated()) {
            Object principal = newAuthentication.getPrincipal();

            if (principal instanceof UserDetails) {
                UserDetails userDetails = (UserDetails) principal;
                response = createResponse(userDetails);
            } else {
                throw new Bug("Unsupported Principal: " + principal);
            }
        } else {
            response = new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        return response;
    }

    @PostMapping(value = "/token-auth", consumes = Headers.CONTENT_TYPE_JSON, produces = Headers.CONTENT_TYPE_JSON)
    public ResponseEntity authenticate(@RequestBody Map<String, String> request) {
        ResponseEntity response;
        String username = request.get(IDENTIFICATION_FIELD);
        String password = request.get(PASSWORD_FIELD);

        if (username == null || password == null) {
            response = ResponseEntity.badRequest().build();
        } else {
            Authentication authentication = new UsernamePasswordAuthenticationToken(username, password);
            response = createResponse(authentication);
        }

        return response;
    }

    @PostMapping(value = "/token-refresh", consumes = Headers.CONTENT_TYPE_JSON, produces = Headers.CONTENT_TYPE_JSON)
    public ResponseEntity refresh(@RequestBody Map<String, String> request) {
        ResponseEntity response;
        String token = request.get(Headers.TOKEN);

        if (token == null) {
            response = ResponseEntity.badRequest().build();
        } else {
            String secret = jwtProperties.getSecret();
            JsonWebToken jwt = new JsonWebToken(token, secret);
            Authentication authentication = new JwtAuthenticationToken(jwt);
            response = createResponse(authentication);
        }

        return response;
    }
}
