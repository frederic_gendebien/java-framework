/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.security.jwt;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.UUID;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.AuthenticationException;

import com.dim3.security.AbstractUserDetails;
import com.dim3.security.UserDetails;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class JsonWebToken extends AbstractUserDetails {

    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

    private static final String LANGUAGE_CLAIM = "lang";

    private static final String USER_ID_CLAIM = "user_id";

    private static final String ROLES_CLAIM = "roles";

    private String username;

    private String password;

    private String language;

    private String userId;

    private final Collection<String> roles = new HashSet<>();

    public JsonWebToken(String key, String secret) throws AuthenticationException {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(key)
                    .getBody();

            username = claims.getSubject();

            if (username == null) {
                throw new BadCredentialsException("No username found in token");
            }

            password = key;
            language = getRequiredClaim(claims, LANGUAGE_CLAIM, String.class);
            userId = getRequiredClaim(claims, USER_ID_CLAIM, String.class);
            roles.addAll(getRequiredClaim(claims, ROLES_CLAIM, Collection.class));
        } catch (ExpiredJwtException e) {
            throw new CredentialsExpiredException("Expired JWT", e);
        } catch (UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e ) {
            throw new BadCredentialsException("JWT validation failed");
        }
    }

    private static <T> T getRequiredClaim(Claims claims, String name, Class<T> type) {
        T claim = claims.get(name, type);

        if (claim == null) {
            throw new BadCredentialsException(name + " claim not found in token");
        }

        return claim;
    }

    public JsonWebToken(UserDetails userDetails, String secret, long validityPeriod) {
        Date creationDate = new Date();
        long creationTime = creationDate.getTime();

        username = userDetails.getUsername();
        password = Jwts.builder()
                .setSubject(username)
                .claim(LANGUAGE_CLAIM, userDetails.getLanguage())
                .claim(USER_ID_CLAIM, userDetails.getUserId() != null ? userDetails.getUserId() : UUID.randomUUID().toString())
                .claim(ROLES_CLAIM, userDetails.getRoles())
                .setIssuedAt(creationDate)
                .setExpiration(new Date(creationTime + validityPeriod))
                .setHeaderParam(Header.TYPE, Header.JWT_TYPE)
                .signWith(SIGNATURE_ALGORITHM, secret)
                .compact();
        language = userDetails.getLanguage();
        userId = userDetails.getUserId();
        roles.addAll(userDetails.getRoles());
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public Collection<String> getRoles() {
        return new HashSet<>(roles);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
