/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.security.jwt.test;

import org.springframework.test.web.servlet.request.RequestPostProcessor;

import com.dim3.Headers;
import com.dim3.security.User;
import com.dim3.security.jwt.JsonWebToken;
import com.dim3.security.jwt.JwtProperties;

public final class JwtMockMvcRequestBuilders {

    private JwtMockMvcRequestBuilders() { }

    public static RequestPostProcessor jwt(JwtProperties jwtProperties) {
        return request -> {
            String secret = jwtProperties.getSecret();
            long validity = jwtProperties.getValidity();
            User user = new User("user", "password");
            JsonWebToken jwt = new JsonWebToken(user, secret, validity);
            request.addHeader(Headers.TOKEN, jwt.getPassword());

            return request;
        };
    }
}
