/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.jsonapi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.StringUtils;

import com.dim3.exception.Bug;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.github.jasminb.jsonapi.ResourceConverter;
import com.github.jasminb.jsonapi.annotations.Type;

final class JsonApiConverterRegistrar implements ImportBeanDefinitionRegistrar, EnvironmentAware, ResourceLoaderAware, BeanFactoryAware {

    private final Logger logger = LoggerFactory.getLogger(JsonApiConverterRegistrar.class);

    private BeanFactory beanFactory;

    private Environment environment;

    private ResourceLoader resourceLoader;

    JsonApiConverterRegistrar() { }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    private void handleBeanDefinition(BeanDefinition beanDefinition, Collection<Class<?>> classes) {
        String className = beanDefinition.getBeanClassName();
        Class<?> clazz = null;

        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new Bug("Json API type class not found: " + className, e);
        }

        classes.add(clazz);
    }

    private Set<BeanDefinition> findClasses(AnnotationMetadata importingClassMetadata) {
        Set<BeanDefinition> beanDefinitions = new HashSet<>();
        Map<String, Object> annotationAttributeMap = importingClassMetadata.getAnnotationAttributes(EnableJsonApiConverter.class.getName());
        AnnotationAttributes annotationAttributes = AnnotationAttributes.fromMap(annotationAttributeMap);
        Set<String> basePackages = new HashSet<>();
        String explicitBasePackage = annotationAttributes.getString("value");

        if (StringUtils.isEmpty(explicitBasePackage)) {
            if (AutoConfigurationPackages.has(beanFactory)) {
                basePackages.addAll(AutoConfigurationPackages.get(beanFactory));
            } else {
                logger.warn("No basePackage specified for @EnableJsonApiConverter and @EnableAutoConfiguration not detected - no resource converter will be created");
            }
        } else {
            basePackages.add(explicitBasePackage);
        }

        if (!basePackages.isEmpty()) {
            ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false, environment);
            scanner.setResourceLoader(resourceLoader);
            scanner.addIncludeFilter(new AnnotationTypeFilter(Type.class));

            for (String basePackage : basePackages) {
                beanDefinitions.addAll(scanner.findCandidateComponents(basePackage));
            }
        }

        return beanDefinitions;
    }

    private static void createResourceConverterBeanDefinition(BeanDefinitionRegistry registry, Collection<Class<?>> classes) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new ParameterNamesModule());
        objectMapper.registerModule(new JavaTimeModule());
        ConstructorArgumentValues constructorArgumentValues = new ConstructorArgumentValues();
        constructorArgumentValues.addIndexedArgumentValue(0, objectMapper);
        constructorArgumentValues.addIndexedArgumentValue(1, classes);
        RootBeanDefinition beanDefinition = new RootBeanDefinition(ResourceConverter.class, constructorArgumentValues, null);
        registry.registerBeanDefinition("resourceConverter", beanDefinition);
    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        Collection<Class<?>> classes = new ArrayList<>();
        Set<BeanDefinition> beanDefinitions = findClasses(importingClassMetadata);

        for (BeanDefinition beanDefinition : beanDefinitions) {
            handleBeanDefinition(beanDefinition, classes);
        }

        if (!classes.isEmpty()) {
            createResourceConverterBeanDefinition(registry, classes);
        }
    }
}
