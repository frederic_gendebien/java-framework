/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.jsonapi;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.util.Assert;

import com.dim3.exception.Bug;
import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.ResourceConverter;
import com.github.jasminb.jsonapi.exceptions.DocumentSerializationException;

public class JsonApiHttpMessageConverter extends AbstractHttpMessageConverter<Object> {

    private static final String TOTAL_RESOURCE_COUNT_META_KEY = "total";

    private final ResourceConverter resourceConverter;

    public JsonApiHttpMessageConverter(ResourceConverter resourceConverter) {
        super(MediaType.valueOf("application/vnd.api+json"));

        Assert.notNull(resourceConverter, "resourceConverter must not be null");
        this.resourceConverter = resourceConverter;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return Iterable.class.isAssignableFrom(clazz) || resourceConverter.isRegisteredType(clazz);
    }

    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
        return false;
    }

    @Override
    protected boolean canRead(MediaType mediaType) {
        return false;
    }

    @Override
    protected Object readInternal(Class<? extends Object> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void writeInternal(Object object, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        byte[] body;

        try {
            if (object instanceof Page) {
                Page page = (Page) object;
                JSONAPIDocument document = new JSONAPIDocument(page.getContent());
                Map<String, Object> meta = new HashMap<>();
                meta.put(TOTAL_RESOURCE_COUNT_META_KEY, page.getTotalElements());
                document.setMeta(meta);
                body = resourceConverter.writeDocumentCollection(document);
            } else if (object instanceof Iterable) {
                Iterable collection = (Iterable) object;
                JSONAPIDocument document = new JSONAPIDocument(collection);
                body = resourceConverter.writeDocumentCollection(document);
            } else {
                JSONAPIDocument document = new JSONAPIDocument(object);
                body = resourceConverter.writeDocument(document);
            }

            OutputStream output = outputMessage.getBody();
            output.write(body);
        } catch (DocumentSerializationException e) {
            throw new Bug("Unable to serialize object", e);
        }
    }
}
