/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.i18n;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.i18n.AbstractLocaleResolver;

import com.dim3.security.AuthenticatedUserDetailsService;
import com.dim3.security.UserDetails;

public class LocaleResolver extends AbstractLocaleResolver {

    private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;

    private final AuthenticatedUserDetailsService authenticatedUserDetailsService;

    public LocaleResolver(AuthenticatedUserDetailsService authenticatedUserDetailsService) {
        this(authenticatedUserDetailsService, DEFAULT_LOCALE);
    }

    public LocaleResolver(AuthenticatedUserDetailsService authenticatedUserDetailsService, Locale defaultLocale) {
        this.authenticatedUserDetailsService = authenticatedUserDetailsService;

        setDefaultLocale(defaultLocale);
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        Locale locale = getDefaultLocale();
        UserDetails userDetails = authenticatedUserDetailsService.getAuthenticatedUserDetails();

        if (userDetails != null && userDetails.getLanguage() != null) {
            locale = new Locale(userDetails.getLanguage());
        } else if (request.getHeader("Accept-Language") != null) {
            locale = request.getLocale();
        }

        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        throw new UnsupportedOperationException();
    }
}
