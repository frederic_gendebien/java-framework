/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.websocket;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.AbstractMessageConverter;

import com.dim3.Headers;
import com.dim3.exception.Bug;
import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.ResourceConverter;
import com.github.jasminb.jsonapi.exceptions.DocumentSerializationException;

final class JsonApiMessageConverter extends AbstractMessageConverter {

    private final ResourceConverter resourceConverter;

    JsonApiMessageConverter(ResourceConverter resourceConverter) {
        super(Headers.MEDIA_TYPE_JSON_API);

        this.resourceConverter = resourceConverter;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return resourceConverter.isRegisteredType(clazz);
    }

    @Override
    protected boolean canConvertTo(Object payload, MessageHeaders headers) {
        return payload == null || super.canConvertTo(payload, headers);
    }

    @Override
    protected boolean canConvertFrom(Message<?> message, Class<?> targetClass) {
        return false;
    }

    @Override
    protected Object convertFromInternal(Message<?> message, Class<?> targetClass, Object conversionHint) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected byte[] convertToInternal(Object payload, MessageHeaders headers, Object conversionHint) {
        byte[] result;
        JSONAPIDocument document = new JSONAPIDocument(payload);

        try {
            if (payload instanceof Iterable) {
                result = resourceConverter.writeDocumentCollection(document);
            } else {
                result = resourceConverter.writeDocument(document);
            }
        } catch (DocumentSerializationException e) {
            throw new Bug("Unable to serialize object " + payload, e);
        }

        return result;
    }
}
