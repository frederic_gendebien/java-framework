/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.websocket;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.util.MimeType;
import org.springframework.util.StringUtils;

import com.dim3.Headers;
import com.dim3.context.RequestContext;
import com.dim3.context.RequestContextHolder;
import com.dim3.event.Event;
import com.dim3.exception.Bug;

final class WebSocketImpl implements WebSocket {

    private static final String NOT_AVAILABLE = "n/a";

    private static final String ID_PROPERTY = "id";

    private static final String REPLY_DESTINATION_FORMAT = "/exchange/amq.direct/%s";

    private static final String BROADCAST_DESTINATION_FORMAT = "/exchange/amq.topic/%s";

    private final Logger logger = LoggerFactory.getLogger(WebSocketImpl.class);

    private final SimpMessagingTemplate messagingTemplate;

    WebSocketImpl(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Override
    public void replyToUser(Object event) {
        RequestContext requestContext = RequestContextHolder.getRequestContext();
        Object usernameValue = requestContext.get(Headers.USER);

        if (usernameValue == null) {
            throw new Bug("No user found in context");
        }

        String username = String.valueOf(usernameValue);
        Object correlationId = requestContext.get(Headers.CLIENT_CORRELATION_ID);

        if (correlationId == null) {
            throw new Bug("No correlationId found in context");
        }

        String destination = String.format(REPLY_DESTINATION_FORMAT, correlationId);
        SimpMessageHeaderAccessor accessor = SimpMessageHeaderAccessor.create();
        accessor.setContentType(MimeType.valueOf(Headers.CONTENT_TYPE_JSON_API));
        messagingTemplate.convertAndSendToUser(username, destination, event, accessor.getMessageHeaders());
        logMessage(event, destination, username);
    }

    @Override
    @Deprecated
    public void notifyUsers(Object event, String... keys) {
        notifyUsers(event, Headers.MEDIA_TYPE_JSON_API, keys);
    }

    @Override
    public void notifyUsers(String... keys) {
        notifyUsers(null, MediaType.APPLICATION_JSON, keys);
    }

    @Override
    public void notifyUsers(Object event, MimeType contentType, String... keys) {
        String key = StringUtils.arrayToDelimitedString(keys, ".");
        String destination = String.format(BROADCAST_DESTINATION_FORMAT, key);
        SimpMessageHeaderAccessor accessor = SimpMessageHeaderAccessor.create();
        accessor.setContentType(contentType);
        messagingTemplate.convertAndSend(destination, event, accessor.getMessageHeaders());
        logMessage(event, destination);
    }

    private void logMessage(Object event, String destination) {
        logMessage(event, destination, NOT_AVAILABLE);
    }

    private void logMessage(Object event, String destination, String user) {
        if (logger.isInfoEnabled()) {
            Object id = NOT_AVAILABLE;
            String type = NOT_AVAILABLE;

            if (event != null) {
                if (PropertyUtils.isReadable(event, ID_PROPERTY)) {
                    try {
                        id = PropertyUtils.getProperty(event, ID_PROPERTY);
                    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        logger.error("Unable to retrieve id from event {}", event);
                    }
                }

                Class<?> clazz = event.getClass();
                Event eventAnnotation = clazz.getAnnotation(Event.class);

                if (eventAnnotation == null) {
                    type = clazz.getName();
                } else {
                    type = eventAnnotation.value();
                }
            }

            logger.info(">>> {} - {} - WebSocket - {} - {}", type, id, user, destination);
        }
    }
}
