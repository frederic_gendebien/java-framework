/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.websocket;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "websocket-broker")
public class WebSocketBrokerProperties {

    private static final String DEFAULT_RELAY = "/exchange";

    private static final String DEFAULT_ENDPOINT = "/stomp";

    private static final String DEFAULT_HOST = "localhost";

    private static final int DEFAULT_PORT = 61613;

    private static final String DEFAULT_USERNAME = "guest";

    private static final String DEFAULT_PASSWORD = "guest";

    private static final boolean DEFAULT_EMBEDDED = false;

    private String relay = DEFAULT_RELAY;

    private String endpoint = DEFAULT_ENDPOINT;

    private String host = DEFAULT_HOST;

    private int port = DEFAULT_PORT;

    private String username = DEFAULT_USERNAME;

    private String password = DEFAULT_PASSWORD;

    private boolean embedded = DEFAULT_EMBEDDED;
}
