/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.websocket;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.SockJsServiceRegistration;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.StompWebSocketEndpointRegistration;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import com.dim3.jsonapi.JsonApiAutoConfiguration;
import com.dim3.security.SecurityProperties;
import com.github.jasminb.jsonapi.ResourceConverter;

@Configuration
@EnableConfigurationProperties(WebSocketBrokerProperties.class)
@ConditionalOnClass(WebSocketHandler.class)
@EnableWebSocketMessageBroker
@AutoConfigureAfter(JsonApiAutoConfiguration.class)
public class WebSocketAutoConfiguration implements WebSocketMessageBrokerConfigurer {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebSocketBrokerProperties properties;

    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    public WebSocket webSocket(SimpMessagingTemplate messagingTemplate) {
        return new WebSocketImpl(messagingTemplate);
    }

    @Bean
    public SecurityClientInboundChannelInterceptor securityClientInboundChannelInterceptor() {
        return new SecurityClientInboundChannelInterceptor(authenticationManager);
    }

    @Bean
    public ServerCorrelationIdClientInboundChannelInterceptor serverCorrelationIdClientInboundChannelInterceptor() {
        return new ServerCorrelationIdClientInboundChannelInterceptor();
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(securityClientInboundChannelInterceptor(), serverCorrelationIdClientInboundChannelInterceptor());
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        boolean embedded = properties.isEmbedded();

        if (embedded) {
            registry.enableSimpleBroker(properties.getRelay());
        } else {
            registry.enableStompBrokerRelay(properties.getRelay())
                    .setRelayHost(properties.getHost())
                    .setRelayPort(properties.getPort())
                    .setSystemLogin(properties.getUsername())
                    .setSystemPasscode(properties.getPassword());
        }
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        StompWebSocketEndpointRegistration registration = registry.setErrorHandler(new WebSocketAuthenticationErrorHandler())
                .addEndpoint(properties.getEndpoint());

        if (securityProperties.isCorsEnabled()) {
            registration = registration.setAllowedOrigins("*");
        }

        SockJsServiceRegistration sockJsRegistration = registration.withSockJS();

        if (!securityProperties.isCorsEnabled()) {
            sockJsRegistration.setSupressCors(true);
        }
    }

    @Configuration
    @ConditionalOnBean(ResourceConverter.class)
    protected static class JsonApiConverterConfiguration implements WebSocketMessageBrokerConfigurer {

        @Autowired
        private ResourceConverter resourceConverter;

        @Bean
        public JsonApiMessageConverter jsonApiMessageConverter() {
            return new JsonApiMessageConverter(resourceConverter);
        }

        @Override
        public void registerStompEndpoints(StompEndpointRegistry registry) {

        }

        @Override
        public boolean configureMessageConverters(List<MessageConverter> messageConverters) {
            messageConverters.add(jsonApiMessageConverter());

            return true;
        }
    }
}
