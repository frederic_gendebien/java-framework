/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.websocket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

import com.dim3.Headers;
import com.dim3.security.jwt.JwtAuthenticationToken;

public class SecurityClientInboundChannelInterceptor implements ChannelInterceptor {

    private static final Set<StompCommand> HEADERLESS_COMMANDS = new HashSet<>(Arrays.asList(
            null, // PING
            StompCommand.BEGIN,
            StompCommand.COMMIT,
            StompCommand.ABORT,
            StompCommand.DISCONNECT
    ));

    private final AuthenticationManager authenticationManager;

    public SecurityClientInboundChannelInterceptor(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        StompCommand command = accessor.getCommand();

        if (!HEADERLESS_COMMANDS.contains(command)) {
            String token = accessor.getFirstNativeHeader(Headers.TOKEN);
            Authentication authentication = new JwtAuthenticationToken(token);
            authentication = authenticationManager.authenticate(authentication);
            accessor.setUser(authentication);
        }

        return message;
    }
}
