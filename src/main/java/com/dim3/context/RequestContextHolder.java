/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.context;

public final class RequestContextHolder {

    private static final class RequestContextThreadLoacal extends InheritableThreadLocal<RequestContext> {

        @Override
        protected RequestContext childValue(RequestContext parentValue) {
            return new RequestContext(parentValue);
        }
    }

    private static final ThreadLocal<RequestContext> CONTEXTS = new RequestContextThreadLoacal();

    private RequestContextHolder() { }

    public static RequestContext getRequestContext() {
         RequestContext requestContext = CONTEXTS.get();

         if (requestContext == null) {
             requestContext = new RequestContext();
             CONTEXTS.set(requestContext);
         }

         return requestContext;
    }

    public static void clearRequestContext() {
        CONTEXTS.remove();
    }
}
