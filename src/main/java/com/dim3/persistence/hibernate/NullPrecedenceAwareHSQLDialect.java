/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.persistence.hibernate;

import org.hibernate.NullPrecedence;
import org.hibernate.dialect.HSQLDialect;

public class NullPrecedenceAwareHSQLDialect extends HSQLDialect {

    @Override
    public String renderOrderByElement(String expression, String collation, String order, NullPrecedence nulls) {
        final StringBuilder orderByElement = new StringBuilder( expression );

        if (collation != null) {
            orderByElement.append(" ").append(collation);
        }

        if (order != null) {
            orderByElement.append(" ").append(order);

            if ("asc".equalsIgnoreCase(order)) {
                orderByElement.append(" nulls first");
            } else if ("desc".equalsIgnoreCase(order)) {
                orderByElement.append(" nulls last");
            }
        }

        return orderByElement.toString();
    }
}
