package com.dim3.logging;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.boot.env.PropertiesPropertySourceLoader;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class LoggingEnvironmentPostProcessor implements EnvironmentPostProcessor {

    private final PropertiesPropertySourceLoader loader = new PropertiesPropertySourceLoader();

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        addPropertySource(environment, "git-properties", "git.properties");
        addPropertySource(environment, "build-info-properties", "META-INF/build-info.properties");
    }

    private void addPropertySource(ConfigurableEnvironment environment, String name, String path) {
        Resource resource = new ClassPathResource(path);

        if (resource.exists()) {
            try {
                PropertySource<?> propertySource = loader.load(name, resource).get(0);
                environment.getPropertySources().addLast(propertySource);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
