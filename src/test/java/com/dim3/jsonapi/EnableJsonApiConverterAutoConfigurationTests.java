/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.jsonapi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.autoconfigure.TestAutoConfigurationPackage;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import com.dim3.jsonapi.test.TestType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.ResourceConverter;

public class EnableJsonApiConverterAutoConfigurationTests {

    private AnnotationConfigApplicationContext context;

    @Before
    public void setUp() {
        context = new AnnotationConfigApplicationContext();
    }

    @After
    public void tearDown() {
        if (context != null) {
            context.close();
        }
    }

    @Test
    public void jsonApiConverterIsCreatedWhenNoExplicitBasePackageIsConfigured() {
        context.register(NoExplicitBasePackageConfiguration.class);
        context.refresh();
        ResourceConverter resourceConverter = context.getBean(ResourceConverter.class);
        assertTrue(resourceConverter.isRegisteredType(TestType.class));
    }

    @Test
    public void jsonApiConverterIsCreatedWhenAnExplicitBasePackageIsConfigured() {
        context.register(ExplicitBasePackageConfiguration.class);
        context.refresh();
        ResourceConverter resourceConverter = context.getBean(ResourceConverter.class);
        assertTrue(resourceConverter.isRegisteredType(TestType.class));
    }

    @Test
    public void datesAreCorrectlySerialized() throws Exception {
        context.register(ExplicitBasePackageConfiguration.class);
        context.refresh();
        ResourceConverter resourceConverter = context.getBean(ResourceConverter.class);
        String id = "id";
        LocalDate date = LocalDate.now();
        TestType object = new TestType(id, date);
        JSONAPIDocument document = new JSONAPIDocument(object);
        byte[] serialized = resourceConverter.writeDocument(document);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        JsonNode node = objectMapper.readTree(serialized);
        LocalDate actual = objectMapper.convertValue(node.get("data").get("attributes").get("date"), LocalDate.class);
        assertEquals(date, actual);
    }

    @Test
    public void constructorParameterNamesAreCorrectlyResolvedDuringDeserialization() throws Exception {
        context.register(ExplicitBasePackageConfiguration.class);
        context.refresh();
        ResourceConverter resourceConverter = context.getBean(ResourceConverter.class);
        String id = "id";
        LocalDate date = LocalDate.now();
        TestType object = new TestType(id, date);
        JSONAPIDocument<TestType> document = new JSONAPIDocument(object);
        byte[] serialized = resourceConverter.writeDocument(document);
        document = resourceConverter.readDocument(serialized, TestType.class);
        TestType actual = document.get();
        assertEquals(object, actual);
    }

    @Configuration
    @TestAutoConfigurationPackage(TestType.class)
    @EnableJsonApiConverter
    protected static class NoExplicitBasePackageConfiguration {

    }

    @Configuration
    @EnableJsonApiConverter("com.dim3.jsonapi.test")
    protected static class ExplicitBasePackageConfiguration {

    }
}
