/*
 * Copyright 2018 Dim3 SA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dim3.websocket;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.tomcat.websocket.WsWebSocketContainer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.servlet.TomcatWebSocketServletWebServerCustomizer;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.web.context.ServerPortInfoApplicationContextInitializer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.RestTemplateXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import com.dim3.Headers;
import com.dim3.security.SecurityAutoConfiguration;
import com.dim3.security.User;
import com.dim3.security.jwt.JsonWebToken;
import com.dim3.security.jwt.JwtAuthenticationProvider;
import com.dim3.security.jwt.JwtAutoConfiguration;
import com.dim3.security.jwt.JwtProperties;

import lombok.Getter;

public class WebSocketAuthenticationTest {

    private AnnotationConfigServletWebServerApplicationContext context = new AnnotationConfigServletWebServerApplicationContext();

    private SockJsClient sockJsClient;

    private JwtProperties jwtProperties;

    @Before
    public void setUp() {
        List<Transport> transports = Arrays.asList(
                new WebSocketTransport(
                        new StandardWebSocketClient(new WsWebSocketContainer())),
                new RestTemplateXhrTransport(new RestTemplate()));
        sockJsClient = new SockJsClient(transports);
        TestPropertyValues.of("server.port:0", "websocket-broker.embedded:true")
                .applyTo(context);
        context.register(TestConfiguration.class);
        new ServerPortInfoApplicationContextInitializer().initialize(context);
        context.refresh();
        jwtProperties = context.getBean(JwtProperties.class);
    }

    @After
    public void tearDown() {
        context.close();
        sockJsClient.stop();
    }

    @Test
    public void connectingToTheBrokerWithACorrectTokenWorksCorrectly() throws Throwable {
        JsonWebToken jwt = new JsonWebToken(new User("user", "user", UUID.randomUUID().toString(), Arrays.asList("ROLE_USER")), jwtProperties.getSecret(), jwtProperties.getValidity());
        String token = jwt.getPassword();
        connect(token);
    }

    @Test
    public void connectingToTheBrokerWithAMalformedTokenReturnsAnAuthenticationError() throws Throwable {
        connectAndExpectUnauthorized("malformed token");
    }

    @Test
    public void connectingToTheBrokerWithAnExpiredTokenReturnsAnAuthenticationError() throws Throwable {
        JsonWebToken jwt = new JsonWebToken(new User("user", "user", UUID.randomUUID().toString(), Arrays.asList("ROLE_USER")), jwtProperties.getSecret(), -1);
        String token = jwt.getPassword();
        connectAndExpectUnauthorized(token);
    }

    @Test
    public void connectingToTheBrokerWithNoTokenReturnsAnAuthenticationError() throws Throwable {
        connectAndExpectUnauthorized(null);
    }

    @Test
    public void connectingToTheBrokerWithWrongJwtSecretReturnsAnAuthenticationError() throws Throwable {
        JsonWebToken jwt = new JsonWebToken(new User("user", "password", UUID.randomUUID().toString(), Arrays.asList("ROLE_USER")), "wrong secret", jwtProperties.getValidity());
        String token = jwt.getPassword();
        connectAndExpectUnauthorized(token);
    }

    private void connectAndExpectUnauthorized(String token) throws Throwable {
        try {
            connect(token);
            fail("Connection should fail");
        } catch (ConnectionError e) {
            String status = e.getStatus();
            String message = e.getMessage();
            assertThat(status, is("401"));
            assertThat(message, is("UNAUTHORIZED"));
        }
    }

    private void connect(String token) throws Throwable {
        String port = context.getEnvironment().getProperty("local.server.port");
        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        final AtomicReference<ConnectionError> connectionErrorHolder = new AtomicReference<>();
        final AtomicReference<Throwable> errorHolder = new AtomicReference<>();
        final CountDownLatch latch = new CountDownLatch(1);
        StompSessionHandler handler = new StompSessionHandlerAdapter() {

            @Override
            public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
                latch.countDown();
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                String message = headers.getFirst("message");
                String status = headers.getFirst("status");
                connectionErrorHolder.set(new ConnectionError(status, message));
                latch.countDown();
            }

            @Override
            public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
                errorHolder.set(exception);
                latch.countDown();
            }

            @Override
            public void handleTransportError(StompSession session, Throwable exception) {
                errorHolder.set(exception);
                latch.countDown();
            }
        };

        StompHeaders headers = new StompHeaders();
        headers.put(Headers.TOKEN, Arrays.asList(token));
        stompClient.connect("ws://localhost:{port}/stomp", (WebSocketHttpHeaders) null, headers, handler, port);
        boolean timeout = !latch.await(5, TimeUnit.SECONDS);

        if (timeout) {
            fail("No response received in a timely fashion");
        }

        ConnectionError connectionError = connectionErrorHolder.get();

        if (connectionError != null) {
            throw connectionError;
        }

        Throwable error = errorHolder.get();

        if (error != null) {
            throw error;
        }
    }

    private static final class ConnectionError extends RuntimeException {

        @Getter private final String status;

        @Getter private final String message;

        public ConnectionError(String status, String message) {
            this.status = status;
            this.message = message;
        }
    }

    @Configuration
    @ImportAutoConfiguration({ SecurityAutoConfiguration.class,
            JwtAutoConfiguration.class,
            WebSocketAutoConfiguration.class,
            DispatcherServletAutoConfiguration.class,
            ServletWebServerFactoryAutoConfiguration.class })
    protected static class TestConfiguration {

        @Autowired
        private JwtAuthenticationProvider jwtAuthenticationProvider;

        @Bean
        public TomcatServletWebServerFactory tomcat() {
            return new TomcatServletWebServerFactory(0);
        }

        @Bean
        public TomcatWebSocketServletWebServerCustomizer tomcatCustomizer() {
            return new TomcatWebSocketServletWebServerCustomizer();
        }

        @Bean
        public ProviderManager providerManager() {
            return new ProviderManager(Arrays.asList(jwtAuthenticationProvider));
        }
    }
}
